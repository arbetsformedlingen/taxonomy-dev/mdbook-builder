# Summary

[Introduction](introduction.md)

---

- [Documentation](mdbook/doc-mdbook.md)
  - [Plugins](mdbook/doc-plugins.md)
    - [Math](mdbook/doc-plugins-math.md)
    - [Graphs](mdbook/doc-plugins-graphs.md)
    - [Notices](mdbook/doc-plugins-notices.md)
