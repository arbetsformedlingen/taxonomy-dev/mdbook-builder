# Introduction

The mdbook builder image is a Docker image that contains the [mdbook](https://rust-lang.github.io/mdBook/) tool and some plugins. It is used to build documentation from markdown files.
