# Notices

## Admonish

```admonish info
Can be used for fancy notices.
```

```admonish warning
Writing software without tests is like skydiving without a parachute.
```
