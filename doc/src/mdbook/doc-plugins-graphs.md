# Graphs

## SVG Bob

```bob
    0       3
     *-------*      +y
  1 /|    2 /|       ^
   *-------* |       |
   | |4    | |7      | ◄╮
   | *-----|-*     ⤹ +-----> +x
   |/      |/       / ⤴
   *-------*       v
  5       6      +z
```

## PlantUML

```puml
@startuml Hello!
Bob->Alice : Hello!
@enduml
```

## Mermaid

```mermaid
mindmap
  root((Mermaid))
    Can be used for graphs
      Graphs
      Diagrams
      Flowcharts
    Just
      Another
        Tool
        Brick
      This
```
