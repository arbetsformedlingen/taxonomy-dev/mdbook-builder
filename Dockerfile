FROM rust:1.81.0-bookworm as builder

RUN apt-get -y update && \
    apt-get install libssl-dev pkg-config -y && \
    cargo install mdbook --no-default-features --features search && \
    cargo install mdbook-plantuml && \
    cargo install mdbook-admonish && \
    cargo install --locked mdbook-svgbob && \
    cargo install mdbook-mermaid && \
    cargo install mdbook-preprocessor-graphql-playground && \
    cargo install --locked mdbook-katex && \
    cargo install mdbook-linkcheck

RUN cargo install --locked nu --features sqlite && \
    cargo install --locked nu_plugin_polars && \
    cargo install --locked bat && \
    cargo install --locked ripgrep && \
    cargo install --locked fd-find && \
    cargo install --locked rsign2 && \
    cargo install --locked parquet --features cli,arrow,flate2,lz4,snap,zstd

FROM ubuntu:24.04 as runner

RUN apt-get -y update && apt install -y openssl && rm -rf /var/lib/apt/lists/*

COPY --from=builder /usr/local/cargo/bin/mdbook /usr/local/bin/mdbook
COPY --from=builder /usr/local/cargo/bin/mdbook-plantuml /usr/local/bin/mdbook-plantuml
COPY --from=builder /usr/local/cargo/bin/mdbook-admonish /usr/local/bin/mdbook-admonish
COPY --from=builder /usr/local/cargo/bin/mdbook-svgbob /usr/local/bin/mdbook-svgbob
COPY --from=builder /usr/local/cargo/bin/mdbook-mermaid /usr/local/bin/mdbook-mermaid
COPY --from=builder /usr/local/cargo/bin/mdbook-katex /usr/local/bin/mdbook-katex
COPY --from=builder /usr/local/cargo/bin/mdbook-linkcheck /usr/local/bin/mdbook-linkcheck
COPY --from=builder /usr/local/cargo/bin/mdbook-graphql-playground /usr/local/bin/mdbook-graphql-playground
COPY --from=builder /usr/local/cargo/bin/nu /usr/local/bin/nu
COPY --from=builder /usr/local/cargo/bin/bat /usr/local/bin/bat
COPY --from=builder /usr/local/cargo/bin/rg /usr/local/bin/rg
COPY --from=builder /usr/local/cargo/bin/fd /usr/local/bin/fd
COPY --from=builder /usr/local/cargo/bin/rsign /usr/local/bin/rsign
COPY --from=builder /usr/local/cargo/bin/parquet-concat /usr/local/bin/parquet-concat
COPY --from=builder /usr/local/cargo/bin/parquet-fromcsv /usr/local/bin/parquet-fromcsv
COPY --from=builder /usr/local/cargo/bin/parquet-index /usr/local/bin/parquet-index
COPY --from=builder /usr/local/cargo/bin/parquet-layout /usr/local/bin/parquet-layout
COPY --from=builder /usr/local/cargo/bin/parquet-read /usr/local/bin/parquet-read
COPY --from=builder /usr/local/cargo/bin/parquet-rewrite /usr/local/bin/parquet-rewrite
COPY --from=builder /usr/local/cargo/bin/parquet-rowcount /usr/local/bin/parquet-rowcount
COPY --from=builder /usr/local/cargo/bin/parquet-schema /usr/local/bin/parquet-schema
COPY --from=builder /usr/local/cargo/bin/parquet-show-bloom-filter /usr/local/bin/parquet-show-bloom-filter


WORKDIR /book
VOLUME ["/book"]

CMD mdbook build /book
